package com.example.farahat.fixawy.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.farahat.fixawy.R;
import com.example.farahat.fixawy.ServiceDetailActivity;

public class ServicesItemAdapter extends
        RecyclerView.Adapter<ServicesItemAdapter.ViewHolder> {
    private String[] name;
    private int[] imageIds;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    public ServicesItemAdapter(String[] captions, int[] imageIds) {
        this.name = captions;
        this.imageIds = imageIds;
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    @Override
    public ServicesItemAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CardView cardView = holder.cardView;
        ImageView imageView = (ImageView) cardView.findViewById(R.id.info_image);
        Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), imageIds[position]);
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(name[position]);
        TextView textView = (TextView) cardView.findViewById(R.id.info_text);
        textView.setText(name[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cardView.getContext(), ServiceDetailActivity.class);
                intent.putExtra(ServiceDetailActivity.EXTRA_SERVICE_ID, position);
                cardView.getContext().startActivity(intent);
            }
        });
    }
}
