package com.example.farahat.fixawy.UI.HomeFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.farahat.fixawy.R;
import com.example.farahat.fixawy.model.Service;
import com.example.farahat.fixawy.adapter.ServicesItemAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment {


    public ServicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RecyclerView ServiceRecycler = (RecyclerView) inflater.inflate(
                R.layout.fragment_services, container, false);

        String[] serviceNames = new String[Service.services.length];
        for (int i = 0; i < serviceNames.length; i++) {
            serviceNames[i] = Service.services[i].getName();
        }
        int[] serviceImages = new int[Service.services.length];
        for (int i = 0; i < serviceImages.length; i++) {
            serviceImages[i] = Service.services[i].getImageResourceId();
        }

        ServicesItemAdapter adapter = new ServicesItemAdapter(serviceNames, serviceImages);
        ServiceRecycler.setAdapter(adapter);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        ServiceRecycler.setLayoutManager(layoutManager);

        return ServiceRecycler;
    }

}
