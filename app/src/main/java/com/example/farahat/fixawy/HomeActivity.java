package com.example.farahat.fixawy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.farahat.fixawy.UI.HomeFragments.PriceFragment;
import com.example.farahat.fixawy.UI.HomeFragments.RequestFragment;
import com.example.farahat.fixawy.UI.HomeFragments.ServicesFragment;
import com.example.farahat.fixawy.UI.HomeFragments.SettingsFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kekstudio.dachshundtablayout.DachshundTabLayout;

public class HomeActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private String userId;
    private FirebaseDatabase mDatabase;
    private DatabaseReference userRef;

    final int[] ICONS = new int[]{
            R.drawable.ic_services,
            R.drawable.ic_transfer,
            R.drawable.ic_price_tag,
            R.drawable.ic_setting
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        PagerAdapter pagerAdapter =
                new PagerAdapter(getSupportFragmentManager());
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);


        DachshundTabLayout tabLayout = (DachshundTabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);
        tabLayout.getTabAt(0).setIcon(ICONS[0]);
        tabLayout.getTabAt(1).setIcon(ICONS[1]);
        tabLayout.getTabAt(2).setIcon(ICONS[2]);
        tabLayout.getTabAt(3).setIcon(ICONS[3]);
        //Attach the ViewPager to the TabLayout
//        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(pager);

        setUserData();

    }


    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ServicesFragment();
                case 1:
                    return new RequestFragment();
                case 2:
                    return new PriceFragment();
                case 3:
                    return new SettingsFragment();
            }
            return null;
        }


        @Override
        public int getCount() {
            return 4;
        }

//        @Override
//        public CharSequence getPageTitle(int position) {
//            switch (position) {
//                case 0:
//                    return getResources().getText(R.string.services_tab);
//                case 1:
//                    return getResources().getText(R.string.request_tab);
//                case 2:
//                    return getResources().getText(R.string.prices_tab);
//                case 3:
//                    return getResources().getText(R.string.settings_tab);
//            }
//            return null;
//        }
    }

    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void setUserData() {

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mUser = mAuth.getCurrentUser();
        userId = mUser.getUid();

        userRef = mDatabase.getReferenceFromUrl(Constants.FIREBASE_URL_USERS).child(userId);
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("name").getValue(String.class);
                String phone = dataSnapshot.child("phone").getValue(String.class);
                String userUID = dataSnapshot.getKey();

                SharedPreferences pref = getApplicationContext().getSharedPreferences("UserData", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("Name", name); // Storing string
                editor.putString("Phone", phone); // Storing string
                editor.putString("Uid", userUID);
                editor.commit(); // commit changes
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
