package com.example.farahat.fixawy.UI.Registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.farahat.fixawy.Constants;
import com.example.farahat.fixawy.HomeActivity;
import com.example.farahat.fixawy.R;
import com.example.farahat.fixawy.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {


    private TextView mSignIn, mCreate;
    private String email, password, phone, name;
    private EditText mEditTextName, mEditTextPhone, mEditTextPassword;
    private static final String TAG = "SignHp Activity";
    private ProgressDialog mAuthProgressDialog;

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initializeScreen();


    }

    public void loginClicked(View view) {
        Intent it = new Intent(SignupActivity.this, SigninActivity.class);
        startActivity(it);
    }

    public void signinClicked(View view) {

        name = (mEditTextName.getText().toString());
        email = name.replaceAll("\\s+", "") + "@fix.com";
        password = mEditTextPassword.getText().toString();
        phone = mEditTextPhone.getText().toString();

        /**
         * Check that email and user name are okay
         */
        boolean validName = isUserNameValid(name);
        boolean validPassword = isPasswordValid(password);
        boolean validPhone = isUserPhoneValid(phone);
        if (!validName || !validPhone || !validPassword) return;

        mAuthProgressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            //       FirebaseUser user = mAuth.getCurrentUser();

                            writeNewUser(name, phone, "said st , Tanta");

                            Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                            mAuthProgressDialog.dismiss();


                        } else {

                            mAuthProgressDialog.dismiss();

                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
//                            Toast.makeText(SignupActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();

                            try {
                                throw task.getException();
                            } catch (FirebaseAuthWeakPasswordException e) {
                                mEditTextPassword.setText("");
                                mEditTextPassword.setHint(R.string.error_invalid_password_not_valid);
                                mEditTextPassword.setHintTextColor(getResources().getColor(R.color.red));
                                mEditTextPassword.requestFocus();
//                                mEditTextPassword.setError(getString(R.string.error_invalid_password_not_valid));
//                                mEditTextPassword.requestFocus();
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                mEditTextName.setText("");
                                mEditTextName.setHint(R.string.error_invalid_email_not_valid);
                                mEditTextName.setHintTextColor(getResources().getColor(R.color.red));
                                mEditTextName.requestFocus();
//                                mEditTextName.setError(String.format(getString(R.string.error_invalid_email_not_valid),
//                                        email));
//                                mEditTextName.requestFocus();
                            } catch (FirebaseAuthUserCollisionException e) {
                                mEditTextName.setText("");
                                mEditTextName.setHint(R.string.error_email_taken);
                                mEditTextName.setHintTextColor(getResources().getColor(R.color.red));
                                mEditTextName.requestFocus();
//                                mEditTextName.setError(getString(R.string.error_email_taken));
//                                mEditTextName.requestFocus();
                            } catch (FirebaseNetworkException e) {
                                showErrorToast(getString(R.string.network_error));
                            } catch (Exception e) {
                                Log.e(TAG, e.getMessage());
                            }

                        }
                    }
                });
    }

    private void writeNewUser(String name, String phone, String s) {

        User user = new User(name, phone, s);
        mDatabaseRef.child(mAuth.getCurrentUser().getUid()).setValue(user);


    }


    private void initializeScreen() {

        //Firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabaseRef = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_URL_USERS);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "myfonts/Roboto-Light.ttf");
        mSignIn = (TextView) findViewById(R.id.signin);
        mCreate = (TextView) findViewById(R.id.create);
        mEditTextName = (EditText) findViewById(R.id.singup_Name);
        mEditTextPhone = (EditText) findViewById(R.id.singup_phone);
        mEditTextPassword = (EditText) findViewById(R.id.singup_password);

        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading));
        mAuthProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_creating_user_with_firebase));
        mAuthProgressDialog.setCancelable(false);

        mSignIn.setTypeface(custom_font);
        mCreate.setTypeface(custom_font);
        mEditTextName.setTypeface(custom_font);
        mEditTextPhone.setTypeface(custom_font);
        mEditTextPassword.setTypeface(custom_font);
    }


    private boolean isUserNameValid(String userName) {
        if (userName.length() < 4 && userName.length() > 0) {
            mEditTextName.setText("");
            mEditTextName.setHint(R.string.error_invalid_name);
            mEditTextName.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextName.setError(getResources().getString(R.string.error_cannot_be_empty));
            return false;
        }
        if (name.length() == 0) {
            mEditTextName.setText("");
            mEditTextName.setHint(R.string.error_cannot_be_empty);
            mEditTextName.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    public boolean isUserPhoneValid(String userPhone) {
        if (userPhone.length() != 11) {
            mEditTextPhone.setText("");
            mEditTextPhone.setHint(R.string.error_invalid_number);
            mEditTextPhone.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextPhone.setError(getResources().getString(R.string.error_invalid_number));
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String password) {
        if (password.length() < 6 && password.length() > 0) {
            mEditTextPassword.setText("");
            mEditTextPassword.setHint(R.string.error_invalid_password_not_valid);
            mEditTextPassword.setHintTextColor(this.getResources().getColor(R.color.red));

            // mEditTextPassword.setError(getResources().getString(R.string.error_invalid_password_not_valid));
            return false;
        }
        if (password.length() == 0) {
            mEditTextPassword.setText("");
            mEditTextPassword.setHint(R.string.error_cannot_be_empty);
            mEditTextPassword.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    private void showErrorToast(String message) {
        Toast.makeText(SignupActivity.this, message, Toast.LENGTH_LONG).show();
    }
}
