package com.example.farahat.fixawy;

public class Constants {

    public static final String FIREBASE_LOCATION_USERS = "users";
    public static final String FIREBASE_LOCATION_REQUESTS = "request";

    public static final String FIREBASE_URL = BuildConfig.UNIQUE_FIREBASE_DATABASE_ROOT_URL;
    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
    public static final String FIREBASE_URL_REQUESTS = FIREBASE_URL + "/" + FIREBASE_LOCATION_REQUESTS;


}
