package com.example.farahat.fixawy.model;

public class Request {

    String name, phone, location, description, uid;
    int job;

    public Request() {
    }

    public Request(String name, String phone, String location, String description, int job) {
        this.name = name;
        this.phone = phone;
        this.location = location;
        this.description = description;
        this.job = job;
    }

    public Request(String name, String phone, String location, String description, String uid, int job) {
        this.name = name;
        this.phone = phone;
        this.location = location;
        this.description = description;
        this.uid = uid;
        this.job = job;
    }

    public Request(String name, String location, int job) {
        this.name = name;
        this.location = location;
        this.job = job;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public int getJob() {
        return job;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setJob(int job) {
        this.job = job;
    }
}
