package com.example.farahat.fixawy.model;


import com.example.farahat.fixawy.R;

public class Product {
    private String title;
    private String shortdesc;
    private String name;
    private String hour;
    private int ImageResourceId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public String getName() {
        return name;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setImageResourceId(int imageResourceId) {
        ImageResourceId = imageResourceId;
    }

    public int getImageResourceId() {
        return ImageResourceId;
    }

    public static Product[] getProducts() {
        return products;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product(String title, String shortdesc, String name, String hour, int imageResourceId) {
        this.title = title;
        this.shortdesc = shortdesc;
        this.name = name;
        this.hour = hour;
        ImageResourceId = imageResourceId;
    }

    public static final Product[] products = {
            new Product("How to Use the Pen Tool\n" +
                    "in Adobe illusrator", "The pen tool is without\n" +
                    "doubt one of the trickest\n" +
                    "tools to use" +
                    "The pen tool is without\n" +
                    "\" +\n" +
                    "                                \"doubt one of the trickest\\n\" +\n" +
                    "                                \"tools to use" +
                    "The pen tool is without\n" +
                    "\" +\n" +
                    "                                \"doubt one of the trickest\\n\" +\n" +
                    "                                \"tools to use", "By John Doe", "Hour", R.drawable.illustrator),

            new Product("The pen tool is without\n" +
                    "doubt one of the trickest\n" +
                    "tools to use", "The pen tool is without\n" +
                    "doubt one of the trickest\n" +
                    "tools to use" +
                    "The pen tool is without\n" +
                    "\" +\n" +
                    "                                \"doubt one of the trickest\\n\" +\n" +
                    "                                \"tools to use", "By John Doe",
                    "Hour",
                    R.drawable.illustrator),
            new Product("The pen tool is without\n" +
                    "doubt one of the trickest\n" +
                    "tools to use",
                    "The pen tool is without\n",
                    "By John Doe",
                    "Hour",
                    R.drawable.illustrator),

            new Product("The pen tool is without\n" +
                    "doubt one of the trickest\n" +
                    "tools to use",
                    "The pen tool is without\n" +
                            "doubt one of the trickest\n" +
                            "tools to use" +
                            "The pen tool is without\n" +
                            "\" +\n" +
                            "                                \"doubt one of the trickest\\n\" +\n" +
                            "                                \"tools to use",
                    "By John Doe",
                    "Hour",
                    R.drawable.adobephotoshop)
    };

}
