package com.example.farahat.fixawy.UI.Registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.farahat.fixawy.HomeActivity;
import com.example.farahat.fixawy.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Farahat on 27/03/2018.
 */

public class SigninActivity extends AppCompatActivity {

    private static final String TAG = "SignInActivity";
    private EditText mName, mPassword;
    private String name, password, email;
    private TextView mSignin, mCreate;
    private ProgressDialog mAuthProgressDialog;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);


        initializeScreen();
        checkAuth();

    }


    public void signinClicked(View view) {
        name = mName.getText().toString();
        password = mPassword.getText().toString();
        email = name.replaceAll("\\s+", "") + "@fix.com";

        boolean validName = isUserNameValid(name);
        boolean validPassword = isPasswordValid(password);
        if (!validName || !validPassword) return;

        mAuthProgressDialog.show();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mAuthProgressDialog.dismiss();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            String uid = task.getResult().getUser().getUid();
                            Log.d("hhhh", uid);
                            FirebaseUser user = mAuth.getCurrentUser();
                            // Toast.makeText(SigninActivity.this, "Welcome" + user.getPhoneNumber(), Toast.LENGTH_SHORT).show();
                            handleLogin();

                        } else {

//                            Toast.makeText(SigninActivity.this, "Error" + task.getException(), Toast.LENGTH_SHORT).show();

                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
//                            Toast.makeText(SigninActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                mPassword.setText("");
                                mPassword.setHint(R.string.error_invalid_password_not_correct);
                                mPassword.setHintTextColor(getResources().getColor(R.color.red));
                                mPassword.requestFocus();
//                                mPassword.setError(getString(R.string.error_invalid_password_not_correct));
//                                mPassword.requestFocus();
                            } catch (FirebaseAuthInvalidUserException e) {

                                mName.setText("");
                                mName.setHint(R.string.error_message_email_issue);
                                mName.setHintTextColor(getResources().getColor(R.color.red));
                                mName.requestFocus();
//                                mName.setError(getString(R.string.error_message_email_issue));
//                                mName.requestFocus();
                            } catch (FirebaseNetworkException e) {
                                showErrorToast(getString(R.string.error_message_failed_sign_in_no_network));
                            } catch (FirebaseTooManyRequestsException e) {
                                Toast.makeText(getBaseContext(), R.string.many_login_requests, Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Log.e(TAG, e + "");
                            }

                        }

                        // ...
                    }
                });
    }

    public void createClicked(View view) {
        Intent intent = new Intent(SigninActivity.this, SignupActivity.class);
        startActivity(intent);
    }

    private void initializeScreen() {

        mAuth = FirebaseAuth.getInstance();
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "myfonts/Roboto-Light.ttf");
        mName = (EditText) findViewById(R.id.singin_name);
        mPassword = (EditText) findViewById(R.id.singin_password);
        mSignin = (TextView) findViewById(R.id.signin_textview);
        mCreate = (TextView) findViewById(R.id.create_textview);
        mName.setTypeface(custom_font);
        mPassword.setTypeface(custom_font);
        mSignin.setTypeface(custom_font);
        mCreate.setTypeface(custom_font);

        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading));
        mAuthProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_authenticating_with_firebase));
        mAuthProgressDialog.setCancelable(false);
    }

    private void checkAuth() {

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    // Toast.makeText(LoginActivity.this, "onAuthStateChanged:signed_in:", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Toast.makeText(SigninActivity.this, user.getUid().toString(), Toast.LENGTH_SHORT).show();
                    handleLogin();
                } else {
                    // User is signed out
                    // Toast.makeText(LoginActivity.this, "onAuthStateChanged:signed_out:", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }

    private void handleLogin() {
        Intent intent = new Intent(SigninActivity.this, HomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private boolean isUserNameValid(String name) {
        if (name.length() < 4 && name.length() > 0) {
            mName.setText("");
            mName.setHint(R.string.error_invalid_name);
            mName.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextName.setError(getResources().getString(R.string.error_cannot_be_empty));
            return false;
        }
        if (name.length() == 0) {
            mName.setText("");
            mName.setHint(R.string.error_cannot_be_empty);
            mName.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String password) {
        if (password.length() < 6 && password.length() > 0) {
            mPassword.setText("");
            mPassword.setHint(R.string.error_invalid_password_not_valid);
            mPassword.setHintTextColor(this.getResources().getColor(R.color.red));

            // mEditTextPassword.setError(getResources().getString(R.string.error_invalid_password_not_valid));
            return false;
        }
        if (password.length() == 0) {
            mPassword.setText("");
            mPassword.setHint(R.string.error_cannot_be_empty);
            mPassword.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    /**
     * Show error toast to users
     */
    private void showErrorToast(String message) {
        Toast.makeText(SigninActivity.this, message, Toast.LENGTH_LONG).show();
    }
}

