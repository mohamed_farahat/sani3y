package com.example.farahat.fixawy.UI.HomeFragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.farahat.fixawy.Constants;
import com.example.farahat.fixawy.R;
import com.example.farahat.fixawy.ServiceDetailActivity;
import com.example.farahat.fixawy.model.Request;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {

    private String currentUserUid;
    private LottieAnimationView noItemImage;
    private TextView noItemText;
    private ProgressBar mLoadBar;
    private RecyclerView mRequestRV;

    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Request, RequestsViewHolder> mRequestAdapter;
    DatabaseReference requestsRef;
    FirebaseRecyclerOptions personsOptions;

    public RequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request, container, false);

        initializeScreen(view);


        mRequestAdapter = new FirebaseRecyclerAdapter<Request, RequestsViewHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(final RequestsViewHolder holder, final int position, final Request request) {

                holder.setName(request.getName());
                holder.setLocation(request.getLocation());
                holder.setDesc(request.getDescription());
                holder.setImage(request.getJob());
                holder.editImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), ServiceDetailActivity.class);
                        intent.putExtra(ServiceDetailActivity.EXTRA_SERVICE_ID, holder.getPosition());
                        intent.putExtra("Location", request.getLocation());
                        intent.putExtra("Description", request.getDescription());
                        intent.putExtra("JobNumber", request.getJob());
                        intent.putExtra("RequestUID", request.getUid());
                        view.getContext().startActivity(intent);

                    }
                });
                holder.deleteImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        handleMakeSure(request.getUid());
                    }
                });

            }

            @Override
            public RequestsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.request_item, parent, false);

                mLoadBar.setVisibility(View.GONE);
                return new RequestsViewHolder(view);
            }
        };

        mRequestRV.setAdapter(mRequestAdapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        mRequestAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        mRequestAdapter.stopListening();


    }

    public class RequestsViewHolder extends RecyclerView.ViewHolder {
        View mView;
        ImageView editImage = (ImageView) itemView.findViewById(R.id.edit_request);
        ImageView deleteImage = (ImageView) itemView.findViewById(R.id.delete_request);

        public RequestsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setName(String title) {
            TextView post_title = (TextView) mView.findViewById(R.id.request_name);
            post_title.setText(title);
        }

        public void setLocation(String location) {
            TextView post_location = (TextView) mView.findViewById(R.id.request_location);
            post_location.setText(location);
        }

        public void setDesc(String desc) {
            TextView post_desc = (TextView) mView.findViewById(R.id.request_desc);
            post_desc.setText(desc);
        }

        public void setImage(int job) {
            ImageView post_image = (ImageView) mView.findViewById(R.id.request_image);
            switch (job) {
                case 0:
                    post_image.setImageResource(R.mipmap.ic_builder);
                    break;
                case 1:
                    post_image.setImageResource(R.mipmap.ic_builder);
                    break;
                case 2:
                    post_image.setImageResource(R.mipmap.ic_plumber);
                    break;
                case 3:
                    post_image.setImageResource(R.mipmap.ic_carpenter);
                    break;
                case 4:
                    post_image.setImageResource(R.mipmap.ic_cleaning);
                    break;
                case 5:
                    post_image.setImageResource(R.mipmap.ic_elevator);
                    break;
                case 6:
                    post_image.setImageResource(R.mipmap.ic_farmer);
                    break;
                case 7:
                    post_image.setImageResource(R.mipmap.ic_health_care);
                    break;
                case 8:
                    post_image.setImageResource(R.mipmap.ic_home_keeper);
                    break;
                case 9:
                    post_image.setImageResource(R.mipmap.ic_painter);
                    break;
                case 10:
                    post_image.setImageResource(R.mipmap.ic_welder);
                    break;
                default:
                    post_image.setImageResource(R.mipmap.ic_builder);
            }
        }
    }

    private void handleMakeSure(final String uid) {
        int result;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        if (isNetworkConnected()) {
                            mLoadBar.setVisibility(View.VISIBLE);
                            DatabaseReference mRequestRef = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.FIREBASE_URL_REQUESTS);
                            String mCurrentUserUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            mRequestRef.child(mCurrentUserUID).child(uid).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.getChildrenCount() == 0) {

                                                noItemImage.setVisibility(View.VISIBLE);
                                                noItemText.setVisibility(View.VISIBLE);
                                                mLoadBar.setVisibility(View.INVISIBLE);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                    mLoadBar.setVisibility(View.GONE);
                                }
                            });
                            break;
                        } else
                            Toast.makeText(getContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        Log.e("No", uid);

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }

    private void initializeScreen(View view) {
        mLoadBar = (ProgressBar) view.findViewById(R.id.load_request);
        mLoadBar.setVisibility(View.VISIBLE);
        noItemImage = (LottieAnimationView) view.findViewById(R.id.no_item_request_image);
        noItemText = (TextView) view.findViewById(R.id.no_item_request_text);


        currentUserUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("request").child(currentUserUid);

        mRequestRV = (RecyclerView) view.findViewById(R.id.request_recycler);
        requestsRef = FirebaseDatabase.getInstance().getReference().child("request").child(currentUserUid);
        mRequestRV.setLayoutManager(new LinearLayoutManager(getContext()));
        personsOptions = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(requestsRef, Request.class).build();

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {

                    noItemImage.setVisibility(View.VISIBLE);
                    noItemText.setVisibility(View.VISIBLE);
                    mLoadBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
//
//    private boolean isAdapterEmpty() {
//
//    }
}
