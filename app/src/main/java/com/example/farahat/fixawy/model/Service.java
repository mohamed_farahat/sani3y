package com.example.farahat.fixawy.model;

import com.example.farahat.fixawy.R;

public class Service {

    private String name;
    private int imageResourceId;

    public static final Service[] services = {
            new Service("Builder", R.mipmap.ic_builder),
            new Service("Plumber", R.mipmap.ic_plumber),
            new Service("Carpenter", R.mipmap.ic_carpenter),
            new Service("Maid", R.mipmap.ic_cleaning),
            new Service("Elevator", R.mipmap.ic_elevator),
            new Service("Farmer", R.mipmap.ic_farmer),
            new Service("Care", R.mipmap.ic_health_care),
            new Service("Home Keeper", R.mipmap.ic_home_keeper),
            new Service("Painter", R.mipmap.ic_painter),
            new Service("Welder", R.mipmap.ic_welder),

    };

    private Service(String name, int imageResourceId) {
        this.name = name;
        this.imageResourceId = imageResourceId;
    }

    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

}
