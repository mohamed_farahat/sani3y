package com.example.farahat.fixawy.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.farahat.fixawy.PriceDetailActivity;
import com.example.farahat.fixawy.R;
import com.example.farahat.fixawy.ServiceDetailActivity;
import com.example.farahat.fixawy.model.Product;

import java.util.List;


public class ProductAdapter extends
        RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private String[] title, miniText, author, time;
    private int[] imageIds;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    public ProductAdapter(String[] title, String[] miniText, String[] author, String[] time, int[] imageIds) {
        this.title = title;
        this.miniText = miniText;
        this.author = author;
        this.time = time;
        this.imageIds = imageIds;
    }

    @Override
    public int getItemCount() {
        return title.length;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.price_item, parent, false);
        return new ProductAdapter.ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CardView cardView = holder.cardView;

        ImageView imageView = (ImageView) cardView.findViewById(R.id.price_icon);
        Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), imageIds[position]);
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(title[position]);

        TextView textViewTitle = (TextView) cardView.findViewById(R.id.textViewTitle);
        textViewTitle.setText(title[position]);

        TextView textViewShortDesc = (TextView) cardView.findViewById(R.id.textViewShortDesc);
        textViewShortDesc.setText(miniText[position]);

        TextView textViewName = (TextView) cardView.findViewById(R.id.textName);
        textViewName.setText(author[position]);

        TextView textViewHour = (TextView) cardView.findViewById(R.id.textHour);
        textViewHour.setText(time[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cardView.getContext(), PriceDetailActivity.class);
                cardView.getContext().startActivity(intent);
            }
        });
    }
}