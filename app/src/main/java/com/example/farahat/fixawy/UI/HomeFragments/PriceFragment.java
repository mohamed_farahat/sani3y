package com.example.farahat.fixawy.UI.HomeFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.farahat.fixawy.R;
import com.example.farahat.fixawy.adapter.ProductAdapter;
import com.example.farahat.fixawy.adapter.ServicesItemAdapter;
import com.example.farahat.fixawy.model.Product;
import com.example.farahat.fixawy.model.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PriceFragment extends Fragment {
    public PriceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RecyclerView ProductRecycler = (RecyclerView) inflater.inflate(
                R.layout.fragment_feed_back, container, false);

        String[] ProductTitle = new String[Product.products.length];
        for (int i = 0; i < ProductTitle.length; i++) {
            ProductTitle[i] = Product.products[i].getTitle();
        }

        String[] ProductShortDesc = new String[Product.products.length];
        for (int i = 0; i < ProductShortDesc.length; i++) {
            ProductShortDesc[i] = Product.products[i].getShortdesc();
        }

        String[] productAuthor = new String[Product.products.length];
        for (int i = 0; i < productAuthor.length; i++) {
            productAuthor[i] = Product.products[i].getName();
        }

        String[] productHours = new String[Product.products.length];
        for (int i = 0; i < productHours.length; i++) {
            productHours[i] = Product.products[i].getHour();
        }
        int[] productImages = new int[Product.products.length];
        for (int i = 0; i < productImages.length; i++) {
            productImages[i] = Product.products[i].getImageResourceId();
        }

        ProductAdapter adapter = new ProductAdapter(ProductTitle, ProductShortDesc, productAuthor, productHours, productImages);
        ProductRecycler.setAdapter(adapter);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        ProductRecycler.setLayoutManager(layoutManager);

        return ProductRecycler;
    }
}