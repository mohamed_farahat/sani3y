package com.example.farahat.fixawy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.EventLogTags;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.farahat.fixawy.model.Request;
import com.example.farahat.fixawy.model.Service;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ServiceDetailActivity extends AppCompatActivity {


    public static final String EXTRA_SERVICE_ID = "serviceId";

    private EditText mNameEditText, mPhoneEditText, mLocationEditText, mDescriptionEditText;
    private TextView mProcessTextView;
    String passUserUid, pushedUid;
    private String nameString, phoneString, locationString, descriptionString, passedLocation, passedDescription, passedUid;
    private int passedJobNumber;
    private String serviceName;
    private ProgressDialog mRequProgressDialog;


    FirebaseDatabase mDatabase;
    DatabaseReference requestRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);

        initializeScreen();
    }

    public void processTextClicked(View view) {

        nameString = mNameEditText.getText().toString();
        phoneString = mPhoneEditText.getText().toString();
        locationString = mLocationEditText.getText().toString();
        descriptionString = mDescriptionEditText.getText().toString();

        boolean validName = isUserNameValid(nameString);
        boolean validPhone = isUserPhoneValid(phoneString);
        boolean validLocation = isLocationValid(locationString);
        boolean validDescription = isDescriptionValid(descriptionString);
        if (!validName || !validPhone || !validLocation || !validDescription) return;


        if (isNetworkConnected()) {

            if (mProcessTextView.getText() == "تأكيد") {
                mRequProgressDialog.show();
                Request request = new Request(nameString, phoneString, locationString, descriptionString, handleJobNum(serviceName));
                requestRef.child(passUserUid).push().setValue(request, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        pushedUid = databaseReference.getKey();

                        requestRef.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                Log.e("UIDIDIDI", pushedUid);
                                requestRef.child(passUserUid).child(pushedUid).child("uid").setValue(pushedUid);
                                Toast.makeText(ServiceDetailActivity.this, "We will Be there soon ..", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ServiceDetailActivity.this, HomeActivity.class);
                                startActivity(intent);
                                mRequProgressDialog.dismiss();

                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }
                });

            } else {
                Request request = new Request(nameString, phoneString, locationString, descriptionString, passedUid, passedJobNumber);
                requestRef.child(passUserUid).child(request.getUid()).setValue(request);
                Intent intent = new Intent(ServiceDetailActivity.this, HomeActivity.class);
                startActivity(intent);
                mRequProgressDialog.dismiss();


            }


        } else
            Toast.makeText(ServiceDetailActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();


    }

    private int handleJobNum(String serviceName) {

        switch (serviceName) {
            case "Builder":
                return 1;
            case "Plumber":
                return 2;
            case "Carpenter":
                return 3;
            case "Maid":
                return 4;
            case "Elevator":
                return 5;
            case "Farmer":
                return 6;
            case "Care":
                return 7;
            case "Home Keeper":
                return 8;
            case "Painter":
                return 9;
            case "Welder":
                return 10;
            default:
                return 0;
        }
    }

    private void initializeScreen() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.details_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mNameEditText = (EditText) findViewById(R.id.name_edittext);
        mPhoneEditText = (EditText) findViewById(R.id.phone_edittext);
        mLocationEditText = (EditText) findViewById(R.id.location_edittext);
        mDescriptionEditText = (EditText) findViewById(R.id.description_border);
        mProcessTextView = (TextView) findViewById(R.id.process_text);

        mRequProgressDialog = new ProgressDialog(this);
        mRequProgressDialog = new ProgressDialog(this);
        mRequProgressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading));
        mRequProgressDialog.setMessage(getResources().getString(R.string.request_add));
        mRequProgressDialog.setCancelable(false);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("UserData", 0); // 0 - for private mode
        String passedName = pref.getString("Name", null);
        String passedPhone = pref.getString("Phone", null);
        passUserUid = pref.getString("Uid", null);

        mNameEditText.setText(passedName); // getting String
        mPhoneEditText.setText(passedPhone);


        Bundle extras = getIntent().getExtras();
        passedLocation = extras.getString("Location");
        passedDescription = extras.getString("Description");
        passedUid = extras.getString("RequestUID");
        passedJobNumber = extras.getInt("JobNumber");


        if (extras != null) {
            if (passedDescription == null) {
                //The key argument here must match that used in the other activity
                mProcessTextView.setText("تأكيد");

                int serviceId = (Integer) getIntent().getExtras().get(EXTRA_SERVICE_ID);
                serviceName = Service.services[serviceId].getName();
                toolbar.setTitle(serviceName);
            } else {
                mProcessTextView.setText("حفظ التغيرات");
                mLocationEditText.setText(passedLocation);
                mDescriptionEditText.setText(passedDescription);
                toolbar.setTitle(handleJobTitle(passedJobNumber));
            }
        }
        mDatabase = FirebaseDatabase.getInstance();
        requestRef = mDatabase.getReferenceFromUrl(Constants.FIREBASE_URL_REQUESTS);


    }

    private boolean isUserNameValid(String name) {
        if (name.length() < 4 && name.length() > 0) {
            mNameEditText.setText("");
            mNameEditText.setHint(R.string.error_invalid_name);
            mNameEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextName.setError(getResources().getString(R.string.error_cannot_be_empty));
            return false;
        }
        if (name.length() == 0) {
            mNameEditText.setText("");
            mNameEditText.setHint(R.string.error_cannot_be_empty);
            mNameEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    public boolean isUserPhoneValid(String Phone) {
        if (Phone.length() != 11) {
            mPhoneEditText.setText("");
            mPhoneEditText.setHint(R.string.error_invalid_number);
            mPhoneEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextPhone.setError(getResources().getString(R.string.error_invalid_number));
            return false;
        }
        return true;
    }

    private boolean isLocationValid(String location) {
        if (location.length() < 10 && location.length() > 0) {
            mLocationEditText.setText("");
            mLocationEditText.setHint(R.string.error_invalid_location);
            mLocationEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextName.setError(getResources().getString(R.string.error_cannot_be_empty));
            return false;
        }
        if (location.length() == 0) {
            mLocationEditText.setText("");
            mLocationEditText.setHint(R.string.error_cannot_be_empty);
            mLocationEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    private boolean isDescriptionValid(String description) {
        if (description.length() < 10 && description.length() > 0) {
            mDescriptionEditText.setText("");
            mDescriptionEditText.setHint(R.string.error_invalid_description);
            mDescriptionEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            // mEditTextName.setError(getResources().getString(R.string.error_cannot_be_empty));
            return false;
        }
        if (description.length() == 0) {
            mDescriptionEditText.setText("");
            mDescriptionEditText.setHint(R.string.error_cannot_be_empty);
            mDescriptionEditText.setHintTextColor(this.getResources().getColor(R.color.red));
            return false;
        }
        return true;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private String handleJobTitle(int serviceNumber) {

        switch (serviceNumber) {
            case 1:
                return "Builder";
            case 2:
                return "Plumber";
            case 3:
                return "Carpenter";
            case 4:
                return "Maid";
            case 5:
                return "Elevator";
            case 6:
                return "Farmer";
            case 7:
                return "Care";
            case 8:
                return "Home Keeper";
            case 9:
                return "Painter";
            case 10:
                return "Welder";
            default:
                return "Fixawy";
        }
    }

}
